# Sample Page Receiver Python

A Message Receiver Sample written in Python designed to demonstrate AMQP Message Collection.

Instructions:
 - git clone https://gitlab.com/tveyes/public/sample-page-receiver-python.git
 - cd sample-page-receiver-python
 - Make sure to set the following values in app.py:
   - brokerPassword
   - brokerHostname
   - brokerPort
   - brokerVirtualHost
   - queueName
 - pip install -r requirements.txt
 - python app.py

If everything is working correctly the application should start dumping data to stdout.

