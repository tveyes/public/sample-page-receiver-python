#!/usr/bin/env python
import pika
import urllib

brokerUsername = "username"
brokerPassword = "password"
brokerHostname = "broker.example.com"
brokerPort = 5672
brokerVirtualHost = "/virtual-host"
queueName = "PageQueue"

brokerUrl = "amqp://{0}:{1}@{2}:{3}/{4}".format(brokerUsername, brokerPassword, brokerHostname, brokerPort, brokerVirtualHost.replace("/", "%2F"))

print(' Connecting to Broker {0}'.format(brokerUrl))

connection = pika.BlockingConnection(
    pika.connection.URLParameters(brokerUrl))
channel = connection.channel()

channel.queue_declare(queue=queueName,passive=True)


def callback(ch, method, properties, body):
    print(" [x] Received %r" % body)


channel.basic_consume(
    queue=queueName, on_message_callback=callback, auto_ack=True)

print(' [*] Waiting for messages. To exit press CTRL+C')
channel.start_consuming()
